
Drupal.behaviors.commonsAnswers = function() {
  
  // Hides comment block, adds a link to expand.
  $('div#comments').hide();
  $('<a href="#" class="commons-answers-comment"></a>').text(Drupal.t('Add comment')).click(function() {
    $('div#comments').toggle();
    return false;
  }).insertBefore($('div#comments'));
}
