<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function questions_and_answers_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'group-question-node';
  $context->description = 'A question node inside a group';
  $context->tag = 'Question';
  $context->conditions = array(
    'context_og_condition_group_type' => array(
      'values' => array(
        'group' => 'group',
      ),
      'options' => array(
        'node_form' => 0,
      ),
    ),
    'node' => array(
      'values' => array(
        'question' => 'question',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-7c026248ccb1231fd6ace9844844578b' => array(
          'module' => 'views',
          'delta' => '7c026248ccb1231fd6ace9844844578b',
          'region' => 'sidebar_last',
          'weight' => 0,
        ),
        'views-question_answers-block_1' => array(
          'module' => 'views',
          'delta' => 'question_answers-block_1',
          'region' => 'content',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('A question node inside a group');
  t('Question');
  $export['group-question-node'] = $context;

  return $export;
}
