<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function questions_and_answers_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: primary-links:content/questions
  $menu_links['primary-links:content/questions'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'content/questions',
    'router_path' => 'content/questions',
    'link_title' => 'Questions',
    'options' => array(),
    'module' => 'commons',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '15',
    'parent_path' => 'community',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Questions');


  return $menu_links;
}
