<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function questions_and_answers_user_default_permissions() {
  $permissions = array();

  // Exported permission: create answer content
  $permissions['create answer content'] = array(
    'name' => 'create answer content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: create question content
  $permissions['create question content'] = array(
    'name' => 'create question content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: delete own answer content
  $permissions['delete own answer content'] = array(
    'name' => 'delete own answer content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: delete own question content
  $permissions['delete own question content'] = array(
    'name' => 'delete own question content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_answer_question
  $permissions['edit field_answer_question'] = array(
    'name' => 'edit field_answer_question',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit own answer content
  $permissions['edit own answer content'] = array(
    'name' => 'edit own answer content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit own question content
  $permissions['edit own question content'] = array(
    'name' => 'edit own question content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: view field_answer_question
  $permissions['view field_answer_question'] = array(
    'name' => 'view field_answer_question',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  return $permissions;
}
