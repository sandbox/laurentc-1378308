<?php

/**
 *
 * @file
 * Tidy node links extension
 */

/**
 *
 */
class QuestionsAndAnswerTidyNodeLinksImpl extends TidyNodeLinksMappingWithThemeFunctions {
  public function get_mapping() {
    $map=parent::get_mapping();
    $map['post_answer']=array(
        'title' => t('Answer'),
        'display_class' => 'TidyNodeLinksElementWithThemeFunctions',
      );
    return $map;
  }
}

