<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function questions_and_answers_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function questions_and_answers_node_info() {
  $items = array(
    'answer' => array(
      'name' => t('Answer'),
      'module' => 'features',
      'description' => t('Answer a question that has been posed by a member of the community.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Answer'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'question' => array(
      'name' => t('Question'),
      'module' => 'features',
      'description' => t('Ask a question that can be voted on and answered by the community.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function questions_and_answers_views_api() {
  return array(
    'api' => '2',
  );
}
